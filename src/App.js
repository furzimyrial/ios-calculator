import React, { Component } from 'react';
import CalcButton from './components/CalcButton';
import CalcDisplay from './components/CalcDisplay';
import './App.css';

class App extends Component {


  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      result: null,
      buffer: '0',
      relation: '',
      hasDot: false,
      hasRelation: false,
      activeClass: "",
    }
  }

  checkRel(x){
    if(x === "." && this.state.hasDot === false){
      this.setState({buffer: this.state.buffer+".",hasDot: true});
    } else if(x === "0"){
      if(this.state.buffer !== "0"){
        this.setState({buffer: this.state.buffer+x});
      }
    } else if (x >= 1 && x <= 9) {
      if(this.state.buffer === "0"){
        this.setState({buffer: x})
      } else {
        this.setState({buffer: this.state.buffer+x});
      }
    } else if (x === "AC") {
      this.setState({buffer: '0', relation: '', result: null,hasDot: false, hasRelation: false});
    } else if(x === "+/-"){
      this.setState({buffer: String(Number(this.state.buffer)*-1)});
    } else if(x === "%") {
      this.setState({buffer: String(Number(this.state.buffer)/100)});
    } else if(x === "+" || x === "-" || x === "*" || x === "/"){
        this.setState({
          relation: x,
          hasRelation: true,
          activeClass: x
        });

    } else if(x === "="){
      if(this.state.relation !== '' && this.state.result !== null)
      this.setState({
        buffer: eval(this.state.result+" "+this.state.relation+" "+this.state.buffer),
        hasRelation: false,
        relation: "",
        result: null,
        activeClass: "",
      });
    }
  }

  handleChange(x){
    if(this.state.hasRelation === true){
      if(this.state.result === null){
        this.setState({result: this.state.buffer,hasRelation: false, buffer: "0"}, () => this.checkRel(x));
      } else {
        this.setState({result: eval(this.state.result+" "+this.state.relation+" "+this.state.buffer),hasRelation: false, buffer: "0"},() => this.checkRel(x));
      }
    } else {
      this.checkRel(x);
    }
    let a = this;
    setTimeout(function () {
      console.log(a.state);
    },100);

  }

  render() {
    return (
        <div className="App">
          <div className="calc-display">
            <CalcDisplay value={this.state.buffer}/>
          </div>
          <div className="calc-keys">
            <div className="calc-row">
              <CalcButton value="AC" colspan="1" color="lightgray" action={() => this.handleChange("AC")}/>
              <CalcButton value="+/-" colspan="1" color="lightgray" action={() => this.handleChange("+/-")}/>
              <CalcButton value="percent" hasIcon="1" colspan="1" color="lightgray" action={() => this.handleChange("%")}/>
              <CalcButton value="divide" hasIcon="1" colspan="1" color="orange" active={(this.state.activeClass === "/") ? "1" : "0"} action={() => this.handleChange("/")}/>
            </div>
            <div className="calc-row">
              <CalcButton value="7" colspan="1" color="gray" action={() => this.handleChange("7")}/>
              <CalcButton value="8" colspan="1" color="gray" action={() => this.handleChange("8")}/>
              <CalcButton value="9" colspan="1" color="gray" action={() => this.handleChange("9")}/>
              <CalcButton value="times" hasIcon="1" colspan="1" color="orange" active={(this.state.activeClass === "*") ? "1" : "0"} action={() => this.handleChange("*")}/>
            </div>
            <div className="calc-row">
              <CalcButton value="4" colspan="1" color="gray" action={() => this.handleChange("4")}/>
              <CalcButton value="5" colspan="1" color="gray" action={() => this.handleChange("5")}/>
              <CalcButton value="6" colspan="1" color="gray" action={() => this.handleChange("6")}/>
              <CalcButton value="minus" hasIcon="1" colspan="1" color="orange" active={(this.state.activeClass === "-") ? "1" : "0"} action={() => this.handleChange("-")}/>
            </div>
            <div className="calc-row">
              <CalcButton value="1" colspan="1" color="gray" action={() => this.handleChange("1")}/>
              <CalcButton value="2" colspan="1" color="gray" action={() => this.handleChange("2")}/>
              <CalcButton value="3" colspan="1" color="gray" action={() => this.handleChange("3")}/>
              <CalcButton value="plus" hasIcon="1" colspan="1" color="orange" active={(this.state.activeClass === "+") ? "1" : "0"} action={() => this.handleChange("+")}/>
            </div>
            <div className="calc-row">
              <CalcButton value="0" colspan="2" color="gray" action={() => this.handleChange("0")}/>
              <CalcButton value="." colspan="1" color="gray" action={() => this.handleChange(".")}/>
              <CalcButton value="equals" hasIcon="1" colspan="1" color="orange" action={() => this.handleChange("=")}/>
            </div>
          </div>
        </div>
    );
  }
}

export default App;

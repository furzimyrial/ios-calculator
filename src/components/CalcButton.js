import React, { Component } from 'react';
import "./CalcButton.css";

class CalcButton extends Component {
  render () {
    return (
      <div className={"calc-button-div calc-col-"+this.props.colspan}>
        <button onClick={this.props.action} className={"calc-button calc-variant-"+this.props.colspan+" calc-"+this.props.color+" "+(this.props.active === "1" ? "calc-orange-active" : "")}>
        {(this.props.hasIcon) ? (<i className={"fa-adjust fas fa-"+this.props.value}></i>) : this.props.value}
        </button>
      </div>
    );
  }
}

export default CalcButton;

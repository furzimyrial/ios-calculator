import React, { Component } from 'react';
import "./CalcDisplay.css";

class CalcDisplay extends Component {

  render () {
    return (
      <div className="calc-display-value">
        {this.props.value}
      </div>
    );
  }
}

export default CalcDisplay;

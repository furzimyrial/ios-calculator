## ReactJS iOS Calculator

iOS Calculator v1.0 made via [ReactJS](https://reactjs.org)

### How to download and install:

* Clone from the gitlab:
`git clone https://gitlab.com/furzimyrial/ios-calculator.git`

* Open the 'ios-calculator' directory:
`cd ios-calculator`

* Install required node packages:
`npm install`

* Start the application:
`npm start`

